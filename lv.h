#if !defined(lv_included)
#define lv_included
#ifndef lv_component_signed
    #define lv_component_signed signed
#endif
#ifndef lv_component_unsigned
    #define lv_component_unsigned unsigned
#endif
#define lv_null ((void*)0)
typedef lv_component_signed   lv_cell;
typedef lv_component_unsigned lv_ucell;
typedef unsigned char         lv_bool;
enum {
    lv_false = 0,
    lv_true = !lv_false
};

typedef struct lv_vector lv_vector;

struct lv_vector {
    lv_ucell x;
    lv_ucell y;
    lv_ucell z;
    lv_ucell w;
};

lv_bool lv_set     (lv_vector *vector,
                    lv_ucell   x,
                    lv_ucell   y,
                    lv_ucell   z,
                    lv_ucell   w);
lv_bool lv_copy    (lv_vector *source,
                    lv_vector *destination);
lv_bool lv_add     (lv_vector *vector1,
                    lv_vector *vector2,
                    lv_vector *destination);
lv_bool lv_subtract(lv_vector *vector1,
                    lv_vector *vector2,
                    lv_vector *destination);
lv_bool lv_multiply(lv_vector *vector1,
                    lv_vector *vector2,
                    lv_vector *destination);
lv_bool lv_divide  (lv_vector *vector1,
                    lv_vector *vector2,
                    lv_vector *destination);
/*
    TODO:
        Things like cross product, normalization, comparison, etc.
*/
#endif
