#include <lq.h>

lq_ucell lq_size = lq_queue_size;

lq_bool lq_new(lq_queue *queue,
               lq_ucell  size) {
    lq_ucell iterator;
    if(queue == lq_null ||
       size  != lq_size) {
        return lq_false;
    }
    for(iterator = 0;
        iterator < lq_size;
        iterator++) {
        queue->data[iterator] = lq_null;
    }
    queue->read_position  = 0;
    queue->write_position = 0;
    queue->count = 0;
    return lq_true;
}

lq_bool lq_enqueue(lq_queue *queue,
                   void     *data) {
    if(queue        == lq_null ||
       data         == lq_null ||
       queue->count >= lq_size) {
        return lq_false;
    }
    queue->data[queue->write_position] = data;
    queue->write_position++;
    queue->count++;
    if(queue->write_position >= lq_size) {
        queue->write_position = 0;
    }
    return lq_true;
}

void* lq_dequeue(lq_queue *queue) {
    void *result;
    if(queue        == lq_null ||
       queue->count == 0) {
        return lq_null;
    }
    result = queue->data[queue->read_position];
    queue->read_position++;
    queue->count--;
    if(queue->read_position >= lq_size) {
        queue->read_position = 0;
    }
    return result;
}

void* lq_peek(lq_queue *queue) {
    void *result;
    if(queue        == lq_null ||
       queue->count == 0) {
        return lq_null;
    }
    result = queue->data[queue->read_position];
    return result;
}
