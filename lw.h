#if !defined(lw_included)
#define lw_included
#include <lm.h>
#if !defined(lw_actor_data_size)
    #define lw_actor_data_size 256
#endif
#if !defined(lw_world_actor_count)
    #define lw_world_actor_count 256
#endif
#define lw_error lw_actor_count
#define lw_null  lm_null
typedef lm_cell  lw_cell;
typedef lm_ucell lw_ucell;
typedef lm_bool  lw_bool;
enum {
    lw_false = lm_false,
    lw_true  = lm_true
};

typedef struct lw_actor lw_actor;
typedef struct lw_world lw_world;

struct lw_actor {
    unsigned char   data[lw_actor_data_size];
    lm_mailbox      mailbox;
    void          (*update)(lw_world*,
                            lw_ucell);
};

struct lw_world {
    lw_actor   actors[lw_world_actor_count];
    lw_bool  (*update)(lw_world*);
};

extern lw_ucell lw_data_size;
extern lw_ucell lw_actor_count;

lw_bool lw_actor_new   (lw_world  *world,
                        void     (*update)(lw_world*,
                                           lw_ucell),
                        lw_ucell   id);
lw_bool lw_actor_delete(lw_world  *world,
                        lw_ucell   id);

lw_bool lw_new(lw_world  *world,
               lw_bool  (*update)(lw_world*),
               lw_ucell   size,
               lw_ucell   count);
lw_bool lw_run(lw_world  *world);
#endif
