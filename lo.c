#include <lo.h>

static lo_object  lo_object_pool_default[lo_object_pool_size];
       lo_object *lo_object_pool = lo_object_pool_default;
       lo_ucell   lo_pool_size   = lo_object_pool_size;

lo_bool lo_aabb_set(lo_aabb   *aabb,
                    lv_vector *position,
                    lv_vector *size) {
    if(aabb     == lo_null ||
       position == lo_null ||
       size     == lo_null) {
        return lo_false;
    }
    lv_copy(&aabb->position, position);
    lv_copy(&aabb->size, size);
    return lo_true;
}

lo_bool lo_aabb_contains(lo_aabb   *aabb,
                         lv_vector *point) {
    if(aabb  == lo_null ||
       point == lo_null) {
        return lo_false;
    }
    if(point->x < aabb->position.x                ||
       point->y < aabb->position.y                ||
       point->z < aabb->position.z                ||
       point->w < aabb->position.w                ||
       point->x > aabb->position.x + aabb->size.x ||
       point->y > aabb->position.y + aabb->size.y ||
       point->z > aabb->position.z + aabb->size.z ||
       point->w > aabb->position.w + aabb->size.w) {
        return lo_false;
    }
    return lo_true;
}

lo_bool lo_aabb_intersects(lo_aabb *aabb1,
                           lo_aabb *aabb2) {
    if(aabb1 == lo_null ||
       aabb2 == lo_null) {
        return lo_false;
    }
    if(aabb1->position.x + aabb1->size.x < aabb2->position.x ||
       aabb1->position.y + aabb1->size.y < aabb2->position.y ||
       aabb1->position.z + aabb1->size.z < aabb2->position.z ||
       aabb1->position.w + aabb1->size.w < aabb2->position.w ||
       aabb1->position.x > aabb2->position.x + aabb2->size.x ||
       aabb1->position.y > aabb2->position.y + aabb2->size.y ||
       aabb1->position.z > aabb2->position.z + aabb2->size.z ||
       aabb1->position.w > aabb2->position.w + aabb2->size.w) {
        return lo_false;
    }
    return lo_true;
}

lo_bool lo_aabb_add(lo_aabb *aabb1,
                    lo_aabb *aabb2,
                    lo_aabb *destination) {
    if(aabb1       == lo_null ||
       aabb2       == lo_null ||
       destination == lo_null) {
        return lo_false;
    }
    lv_add(&aabb1->position, &aabb2->position, &destination->position);
    lv_add(&aabb1->size, &aabb2->size, &destination->size);
    return lo_true;
}

lo_bool lo_aabb_subtract(lo_aabb *aabb1,
                         lo_aabb *aabb2,
                         lo_aabb *destination) {
    if(aabb1       == lo_null ||
       aabb2       == lo_null ||
       destination == lo_null) {
        return lo_false;
    }
    lv_subtract(&aabb1->position, &aabb2->position, &destination->position);
    lv_subtract(&aabb1->size, &aabb2->size, &destination->size);
    return lo_true;
}

lo_bool lo_aabb_multiply(lo_aabb *aabb1,
                         lo_aabb *aabb2,
                         lo_aabb *destination) {
    if(aabb1       == lo_null ||
       aabb2       == lo_null ||
       destination == lo_null) {
        return lo_false;
    }
    lv_multiply(&aabb1->position, &aabb2->position, &destination->position);
    lv_multiply(&aabb1->size, &aabb2->size, &destination->size);
    return lo_true;
}

lo_bool lo_aabb_divide(lo_aabb *aabb1,
                       lo_aabb *aabb2,
                       lo_aabb *destination) {
    if(aabb1       == lo_null ||
       aabb2       == lo_null ||
       destination == lo_null) {
        return lo_false;
    }
    lv_divide(&aabb1->position, &aabb2->position, &destination->position);
    lv_divide(&aabb1->size, &aabb2->size, &destination->size);
    return lo_true;
}

lo_bool lo_init(lo_ucell size) {
    lo_ucell iterator;
    if(size != lo_pool_size) {
        return lo_false;
    }
    for(iterator = 0; iterator < lo_pool_size; iterator++) {
        lo_object_pool[iterator].data      = lo_null;
        lo_object_pool[iterator].type      = 0;
        lo_object_pool[iterator].allocated = lo_false;
        lv_set(&lo_object_pool[iterator].aabb.position, 0, 0, 0, 0);
        lv_set(&lo_object_pool[iterator].aabb.size, 0, 0, 0, 0);
    }
    return lo_true;
}

lo_ucell lo_object_pool_new(void) {
    lo_ucell iterator;
    for(iterator = 0; iterator < lo_pool_size; iterator++) {
        if(lo_object_pool[iterator].allocated == lo_false) {
            return iterator;
        }
    }
    return lo_error;
}

lo_ucell lo_new(void     *data,
                lo_ucell  type) {
    lo_ucell object;
    object = lo_object_pool_new();
    if(object == lo_error) {
        return lo_error;
    }
    lo_object_pool[object].data      = data;
    lo_object_pool[object].type      = type;
    lo_object_pool[object].allocated = lo_true;
    lv_set(&lo_object_pool[object].aabb.position, 0, 0, 0, 0);
    lv_set(&lo_object_pool[object].aabb.size, 0, 0, 0, 0);
    return object;
}

lo_bool lo_delete(lo_ucell object) {
    if(object >= lo_pool_size) {
        return lo_false;
    }
    lo_object_pool[object].allocated = lo_false;
    return lo_true;
}
