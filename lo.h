#if !defined(lo_included)
#define lo_included
#include <lv.h>
#if !defined(lo_object_pool_size)
    #define lo_object_pool_size 256
#endif
#define lo_error lo_pool_size
#define lo_null  lv_null
typedef signed   lo_cell;
typedef unsigned lo_ucell;
typedef lv_bool  lo_bool;
enum {
    lo_false = lv_false,
    lo_true  = lv_true
};

typedef struct lo_aabb   lo_aabb;
typedef struct lo_object lo_object;

struct lo_aabb {
    lv_vector position;
    lv_vector size;
};

struct lo_object {
    void     *data;
    lo_aabb   aabb;
    lo_ucell  type;
    lo_bool   allocated;
};

extern lo_object *lo_object_pool;
extern lo_ucell   lo_pool_size;

lo_bool lo_aabb_set       (lo_aabb   *aabb,
                           lv_vector *position,
                           lv_vector *size);
lo_bool lo_aabb_contains  (lo_aabb   *aabb,
                           lv_vector *point);
lo_bool lo_aabb_intersects(lo_aabb   *aabb1,
                           lo_aabb   *aabb2);
lo_bool lo_aabb_add       (lo_aabb   *aabb1,
                           lo_aabb   *aabb2,
                           lo_aabb   *destination);
lo_bool lo_aabb_subtract  (lo_aabb   *aabb1,
                           lo_aabb   *aabb2,
                           lo_aabb   *destination);
lo_bool lo_aabb_multiply  (lo_aabb   *aabb1,
                           lo_aabb   *aabb2,
                           lo_aabb   *destination);
lo_bool lo_aabb_divide    (lo_aabb   *aabb1,
                           lo_aabb   *aabb2,
                           lo_aabb   *destination);

lo_bool  lo_init           (lo_ucell  size);
lo_ucell lo_object_pool_new(void);
lo_ucell lo_new            (void     *data,
                            lo_ucell  type);
lo_bool  lo_delete         (lo_ucell object);
#endif
