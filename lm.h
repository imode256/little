#if !defined(lm_included)
#define lm_included
#include <lq.h>
#if !defined(lm_message_pool_size)
    #define lm_message_pool_size 256
#endif
#define lm_error lm_pool_size
#define lm_null  lq_null
typedef lq_cell  lm_cell;
typedef lq_ucell lm_ucell;
typedef lq_bool  lm_bool;
enum {
    lm_false = lq_false,
    lm_true  = lq_true
};

typedef struct lm_mailbox lm_mailbox;
typedef struct lm_message lm_message;

struct lm_mailbox {
    lq_queue input;
    lq_queue output;
};

struct lm_message {
    void     *data;
    lm_ucell  sender;
    lm_ucell  receiver;
    lm_ucell  type;
    lm_ucell  index;
    lm_ucell  status;
    lm_bool   allocated;
};

extern lm_message *lm_message_pool;
extern lm_ucell    lm_pool_size;

lm_ucell lm_message_pool_new(void);
lm_ucell lm_message_new     (void     *data,
                             lm_ucell  message,
                             lm_ucell  sender,
                             lm_ucell  receiver,
                             lm_ucell  type);
lm_bool  lm_message_delete  (lm_ucell  message);

lm_bool  lm_init    (lm_ucell    size);
lm_bool  lm_new     (lm_mailbox *mailbox);
lm_ucell lm_peek    (lm_mailbox *mailbox,
                     lm_bool     channel);
lm_bool  lm_send    (lm_mailbox *mailbox,
                     lm_ucell    message);
lm_ucell lm_receive (lm_mailbox *mailbox);
lm_ucell lm_pull    (lm_mailbox *mailbox);
lm_bool  lm_transfer(lm_mailbox *sender,
                     lm_mailbox *receiver);
#endif

