#include <lm.h>

static lm_message  lm_message_pool_default[lm_message_pool_size];
       lm_message *lm_message_pool = lm_message_pool_default;
       lm_ucell    lm_pool_size    = lm_message_pool_size;

lm_ucell lm_message_pool_new(void) {
    lm_ucell iterator;
    for(iterator = 0; iterator < lm_pool_size; iterator++) {
        if(lm_message_pool[iterator].allocated == lm_false) {
            return iterator;
        }
    }
    return lm_error;
}

lm_ucell lm_message_new(void     *data,
                        lm_ucell  message,
                        lm_ucell  sender,
                        lm_ucell  receiver,
                        lm_ucell  type) {
    if(message >= lm_pool_size) {
        message = lm_message_pool_new();
    }
    if(message == lm_error) {
        return lm_error;
    }
    lm_message_pool[message].data      = data;
    lm_message_pool[message].sender    = sender;
    lm_message_pool[message].receiver  = receiver;
    lm_message_pool[message].type      = type;
    lm_message_pool[message].status    = 0;
    lm_message_pool[message].allocated = lm_true;
    return message;
}

lm_bool lm_message_delete(lm_ucell message) {
    if(message >= lm_pool_size) {
        return lm_false;
    }
    lm_message_pool[message].allocated = lm_false;
    return lm_true;
}

lm_bool lm_init(lm_ucell size) {
    lm_ucell iterator;
    if(size != lm_pool_size) {
        return lm_false;
    }
    for(iterator = 0; iterator < lm_pool_size; iterator++) {
        lm_message_pool[iterator].data      = lm_null;
        lm_message_pool[iterator].sender    = lm_error;
        lm_message_pool[iterator].receiver  = lm_error;
        lm_message_pool[iterator].type      = 0;
        lm_message_pool[iterator].index     = iterator;
        lm_message_pool[iterator].status    = 0;
        lm_message_pool[iterator].allocated = lm_false;
    }
    return lm_true;
}

lm_bool lm_new(lm_mailbox *mailbox) {
    if(mailbox == lm_null) {
        return lm_false;
    }
    if(lq_new(&mailbox->input, lq_queue_size) == lm_false) {
        return lm_false;
    }
    return lq_new(&mailbox->output, lq_queue_size);
}

lm_ucell lm_peek(lm_mailbox *mailbox,
                 lm_bool     channel) {
    lm_message *message;
    if(mailbox == lm_null) {
        return lm_error;
    }
    if(channel == lm_false) {
        message = lq_peek(&mailbox->output);
    }
    else {
        message = lq_peek(&mailbox->input);
    }
    if(message == lm_null) {
        return lm_error;
    }
    return message->index;
}

lm_bool lm_send(lm_mailbox *mailbox,
                lm_ucell    message) {
    if(mailbox == lm_null ||
       message >= lm_pool_size) {
        return lm_false;
    }
    return lq_enqueue(&mailbox->output, &lm_message_pool[message]);
}

lm_ucell lm_receive(lm_mailbox *mailbox) {
    lm_message *message;
    if(mailbox == lm_null) {
        return lm_error;
    }
    message = lq_dequeue(&mailbox->input);
    if(message == lm_null) {
        return lm_error;
    }
    return message->index;
}

lm_ucell lm_pull(lm_mailbox *mailbox) {
    lm_message *message;
    if(mailbox == lm_null) {
        return lm_error;
    }
    message = lq_dequeue(&mailbox->output);
    if(message == lm_null) {
        return lm_error;
    }
    return message->index;
}

lm_bool lm_transfer(lm_mailbox *sender,
                    lm_mailbox *receiver) {
    lm_message *message;
    if(sender   == lm_null ||
       receiver == lm_null) {
        return lm_false;
    }
    message = lq_dequeue(&sender->output);
    if(message == lm_null) {
        return lm_false;
    }
    return lq_enqueue(&receiver->input, message);
}
