#include <lv.h>

lv_bool lv_set(lv_vector *vector,
               lv_ucell   x,
               lv_ucell   y,
               lv_ucell   z,
               lv_ucell   w) {
    if(vector == lv_null) {
        return lv_false;
    }
    vector->x = x;
    vector->y = y;
    vector->z = z;
    vector->w = w;
    return lv_true;
}

lv_bool lv_copy(lv_vector *source,
                lv_vector *destination) {
    if(source      == lv_null ||
       destination == lv_null) {
        return lv_false;
    }
    destination->x = source->x;
    destination->y = source->y;
    destination->z = source->z;
    destination->w = source->w;
    return lv_true;
}

lv_bool lv_add(lv_vector *vector1,
               lv_vector *vector2,
               lv_vector *destination) {
    if(vector1     == lv_null ||
       vector2     == lv_null ||
       destination == lv_null) {
        return lv_false;
    }
    destination->x = vector1->x + vector2->x;
    destination->y = vector1->y + vector2->y;
    destination->z = vector1->z + vector2->z;
    destination->w = vector1->w + vector2->w;
    return lv_true;
}

lv_bool lv_subtract(lv_vector *vector1,
                    lv_vector *vector2,
                    lv_vector *destination) {
    if(vector1     == lv_null ||
       vector2     == lv_null ||
       destination == lv_null) {
        return lv_false;
    }
    destination->x = vector1->x - vector2->x;
    destination->y = vector1->y - vector2->y;
    destination->z = vector1->z - vector2->z;
    destination->w = vector1->w - vector2->w;
    return lv_true;
}

lv_bool lv_multiply(lv_vector *vector1,
                    lv_vector *vector2,
                    lv_vector *destination) {
    if(vector1     == lv_null ||
       vector2     == lv_null ||
       destination == lv_null) {
        return lv_false;
    }
    destination->x = vector1->x * vector2->x;
    destination->y = vector1->y * vector2->y;
    destination->z = vector1->z * vector2->z;
    destination->w = vector1->w * vector2->w;
    return lv_true;
}

lv_bool lv_divide(lv_vector *vector1,
                  lv_vector *vector2,
                  lv_vector *destination) {
    if(vector1     == lv_null ||
       vector2     == lv_null ||
       destination == lv_null ||
       vector2->x  == 0       ||
       vector2->y  == 0       ||
       vector2->z  == 0       ||
       vector2->w  == 0) {
        return lv_false;
    }
    destination->x = vector1->x / vector2->x;
    destination->y = vector1->y / vector2->y;
    destination->z = vector1->z / vector2->z;
    destination->w = vector1->w / vector2->w;
    return lv_true;
}
