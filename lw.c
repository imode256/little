#include <lw.h>

lw_ucell lw_data_size   = lw_actor_data_size;
lw_ucell lw_actor_count = lw_world_actor_count;

lw_bool lw_actor_new(lw_world  *world,
                     void     (*update)(lw_world*,
                                        lw_ucell),
                     lw_ucell   id) {
    lw_ucell iterator;
    if(world  == lw_null ||
       update == lw_null ||
       id     >= lw_actor_count) {
        return lw_false;
    }
    if(world->actors[id].update != lw_null) {
        return lw_false;
    }
    for(iterator = 0; iterator < lw_data_size; iterator++) {
        world->actors[id].data[iterator] = 0;
    }
    world->actors[id].update = update;
    return lm_new(&world->actors[id].mailbox);
}

lw_bool lw_actor_delete(lw_world *world,
                        lw_ucell  id) {
    if(world == lw_null ||
       id    >= lw_actor_count) {
        return lw_false;
    }
    if(world->actors[id].update == lw_null) {
        return lw_false;
    }
    world->actors[id].update = lw_null;
    return lw_true;
}

lw_bool lw_new(lw_world  *world,
               lw_bool  (*update)(lw_world*),
               lw_ucell   size,
               lw_ucell   count) {
    lw_ucell iterator;
    if(world                         == lw_null ||
       update                        == lw_null ||
       lw_data_size                  != size    ||
       lw_actor_count                != count   ||
       lm_init(lm_message_pool_size) == lm_false) {
        return lw_false;
    }
    world->update = update;
    for(iterator = 0; iterator < lw_actor_count; iterator++) {
        world->actors[iterator].update = lw_null;
        if(lm_new(&world->actors[iterator].mailbox) == lw_false) {
            return lw_false;
        }
    }
    return lw_true;
}

lw_bool lw_run(lw_world *world) {
    if(world         == lw_null ||
       world->update == lw_null) {
        return lw_false;
    }
    while(world->update(world) == lw_true) {
        continue;
    }
    return lw_true;
}

