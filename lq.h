#if !defined(lq_included)
#define lq_included
#if !defined(lq_queue_size)
    #define lq_queue_size 256
#endif
#define lq_error lq_size
#define lq_null  ((void*)0)
typedef unsigned      lq_ucell;
typedef signed        lq_cell;
typedef unsigned char lq_bool;
enum {
    lq_false = 0,
    lq_true  = !lq_false
};

typedef struct lq_queue lq_queue;

struct lq_queue {
    void     *data[lq_queue_size];
    lq_ucell  read_position;
    lq_ucell  write_position;
    lq_ucell  count;
};

extern lq_ucell lq_size;

lq_bool lq_new    (lq_queue *queue,
                   lq_ucell  size);
lq_bool lq_enqueue(lq_queue *queue,
                   void     *data);
void*   lq_dequeue(lq_queue *queue);
void*   lq_peek   (lq_queue *queue);
#endif
